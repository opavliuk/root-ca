# root-ca

The script for creating your self-signed SSL certificates which signed your Certificate Authority (CA).

This is for `Connection is secure` in your develop environments:

![alt image](https://gitlab.com/opavliuk/root-ca/-/raw/master/screenshots/image.png)

## Usage

`Before create, change information about your company in openssl.cnf!`

```sh
./create_certs [[ domain ]]
```

## Adding trusted root certificates to the server
If you want to send or receive messages signed by root authorities and these authorities are not installed on the server, you must add a trusted root certificate manually.

Use the following steps to add or remove trusted root certificates to/from a server.

### Mac OS X
Use command:
```sh
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ~/new-root-certificate.crt
```
Remove	
Use command:
```sh
sudo security delete-certificate -c "<name of existing certificate>"
```

### Windows
Use command:
```sh
certutil -addstore -f "ROOT" new-root-certificate.crt
```
Remove	
Use command:
```sh
certutil -delstore "ROOT" serial-number-hex
```

### Linux (Ubuntu, Debian)
Copy your CA to dir /usr/local/share/ca-certificates/
Use command:
```sh
sudo cp foo.crt /usr/local/share/ca-certificates/foo.crt
```
Update the CA store:
```sh
sudo update-ca-certificates
```
Remove	
Remove your CA.
```sh
rm /usr/local/share/ca-certificates/foo.crt
```
Update the CA store:
```sh
sudo update-ca-certificates --fresh
```

NOTE
`Restart Kerio Connect to reload the certificates in the 32-bit versions or Debian 7.`

### Linux (CentOs 6)
Install the ca-certificates package:
```sh
yum install ca-certificates
```
Enable the dynamic CA configuration feature:
```sh
update-ca-trust force-enable
```
Add it as a new file to /etc/pki/ca-trust/source/anchors/:
```sh
cp foo.crt /etc/pki/ca-trust/source/anchors/
```
Use command:
```sh
update-ca-trust extract
```

NOTE
`Restart Kerio Connect to reload the certificates in the 32-bit version.`

### Linux (CentOs 5)
Append your trusted certificate to file /etc/pki/tls/certs/ca-bundle.crt
```sh
cat foo.crt >>/etc/pki/tls/certs/ca-bundle.crt
```

NOTE
`Restart Kerio Connect to reload the certificates in the 32-bit version.`


## Create the intermediate pair

https://jamielinux.com/docs/openssl-certificate-authority/create-the-intermediate-pair.html

